FROM node:15.12.0-alpine3.10
RUN mkdir /app
WORKDIR /app
COPY package.json /app
COPY . /app
RUN npm install
RUN npm run build
CMD PORT=32000 npm run start
